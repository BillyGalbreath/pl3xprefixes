package net.pl3x.bukkit.prefixes.listeners;

import br.com.devpaulo.legendchat.api.events.ChatMessageEvent;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.permission.Permission;
import net.pl3x.bukkit.prefixes.Logger;
import net.pl3x.bukkit.prefixes.hook.Vault;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class PlayerListener implements Listener {
    @EventHandler
    private void onChat(ChatMessageEvent event) {
        if (!event.getTags().contains("dwdprefix") && !event.getTags().contains("dwdsuffix")) {
            return;
        }

        Logger.debug("DwD Prefix/Suffix Found!");

        Player player = event.getSender();
        World world = player.getWorld();
        Permission perms = Vault.getPermission();
        Chat chat = Vault.getChat();

        String[] groups = perms.getPlayerGroups(player);
        String prefixes = "";
        String suffixes = "";

        for (String groupName : groups) {
            prefixes += chat.getGroupPrefix(world, groupName);
            suffixes += chat.getGroupSuffix(world, groupName);
        }

        if (event.getTags().contains("dwdprefix")) {
            event.setTagValue("dwdprefix", prefixes);
        }
        if (event.getTags().contains("dwdsuffix")) {
            event.setTagValue("dwdsuffix", suffixes);
        }
    }
}
