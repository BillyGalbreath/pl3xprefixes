package net.pl3x.bukkit.prefixes.configuration;

import net.pl3x.bukkit.prefixes.Main;

public enum Config {
    COLOR_LOGS(true),
    DEBUG_MODE(false);

    private Main plugin;
    private final Object def;

    private Config(Object def) {
        this.plugin = Main.getPlugin(Main.class);
        this.def = def;
    }

    public String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    public boolean getBoolean() {
        return plugin.getConfig().getBoolean(getKey(), (Boolean) def);
    }
}
