package net.pl3x.bukkit.prefixes;

import net.pl3x.bukkit.prefixes.listeners.PlayerListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.Metrics;

import java.io.IOException;

public class Main extends JavaPlugin {
    @Override
    public void onEnable() {
        saveDefaultConfig();

        PluginManager pm = Bukkit.getPluginManager();

        if (!pm.isPluginEnabled("Vault")) {
            Logger.error("Vault is a required plugin dependency! Disabling!");
            pm.disablePlugin(this);
            return;
        }

        pm.registerEvents(new PlayerListener(), this);

        try {
            Metrics metrics = new Metrics(this);
            metrics.start();
        } catch (IOException e) {
            Logger.warn("&4Failed to start Metrics: &e" + e.getMessage());
        }

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        Logger.info(getName() + " disabled!");
    }
}
